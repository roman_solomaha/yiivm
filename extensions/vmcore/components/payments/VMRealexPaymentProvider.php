<?php
/**
 * @class VMRealexPaymentProvider
 * Description of VMRealexPaymentProvider class
 *
 * @author Nikita Kolosov <nkolosov@voodoo-mobile.com>
 */
class VMRealexPaymentProvider extends VMBasePaymentProvider {
	/**
	 * Realex merchant id
	 *
	 * @var int $merchantId
	 */
	public $merchantId;

	/**
	 * Realex secret
	 *
	 * @var string $secret
	 */
	public $secret;

	/**
	 * Information about customer credit card
	 *
	 * @var VMCreditCard $creditCard
	 */
	public $creditCard;

	/**
	 * Amount of payment
	 *
	 * @var VMAmount $amount
	 */
	public $amount;

	public $country;

	/**
	 * @string Realex successful code
	 */
	const CODE_SUCCESSFUL = '00';

	public $result;

	public function __construct($options = array()) {
		$this->setOptions($options);
	}

	/**
	 * @param VMAmount $amount
	 */
	public function setAmount($amount) {
		$amount->amount = $this->getFormattedAmount($amount->amount);
		$this->amount = $amount;
	}

	/**
	 * @param VMCreditCard $creditCard
	 */
	public function setCardDetails($creditCard) {
		$creditCard->type = $this->getFormattedCreditCardType($creditCard->type);
		$this->creditCard = $creditCard;
	}

	private function prepareRequest($orderId) {
		$timestamp = strftime("%Y%m%d%H%M%S");

		$tmpHash = md5(sprintf('%s.%s.%s.%s.%s.%s',
				$timestamp,
				$this->merchantId,
				$orderId,
				$this->amount->amount,
				$this->amount->currency,
				$this->creditCard->number)
		);

		$hash = md5(sprintf('%s.%s', $tmpHash, $this->secret));

		$request = new SimpleXMLElement("<request></request>");
		$request->addAttribute('type', 'auth');
		$request->addAttribute('timestamp', $timestamp);

		$request->addChild('merchantid', $this->merchantId);
		$request->addChild('orderid', $orderId);
		$amount = $request->addChild('amount', $this->amount->amount);
		$amount->addAttribute('currency', $this->amount->currency);

		$card = $request->addChild('card');
		$card->addChild('number', $this->creditCard->number);
		$card->addChild('expdate', sprintf('%s%s', $this->creditCard->expireMonth, mb_substr($this->creditCard->expireYear, 2)));
		$card->addChild('type', $this->creditCard->type);
		$card->addChild('chname', sprintf('%s %s', $this->creditCard->firstName, $this->creditCard->lastName));

		$autosettle = $request->addChild('autosettle');
		$autosettle->addAttribute('flag', 1);
		$request->addChild('md5hash', $hash);
		$tss = $request->addChild('tssinfo');
		$address = $tss->addChild('address');
		$address->addAttribute('type', 'billing');
		$address->addChild('country', $this->country);

		return $request->asXML();
	}

	private function getFormattedAmount($amount) {
		return $amount * 100;
	}

	private function getFormattedCreditCardType($type) {
		try {
			return VMCreditCardType::get($type)->formatted;
		} catch (Exception $e) {
			throw new CException(Yii::t('vmcore.payments', 'Card type "{type}" not supported', array('{type}' => $type)));
		}
	}

	private function parseResponse($xml) {
		$response = new SimpleXMLElement($xml);

		$this->result = (string) $response->result;
		$this->paymentId = $response->orderid;

		if($this->result != self::CODE_SUCCESSFUL) {
			$this->errors = (string) $response->message;
		}
	}

	public function sendPayment($orderId) {

		$request = $this->prepareRequest($orderId);

		// Send the request array to Realex Payments
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://epage.payandshop.com/epage-remote.cgi");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_USERAGENT, "payandshop.com php version 0.9");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); // this line makes it work under https
		$response = curl_exec($ch);

		curl_close($ch);

		$this->parseResponse($response);

		return (!$this->errors);
	}


} 