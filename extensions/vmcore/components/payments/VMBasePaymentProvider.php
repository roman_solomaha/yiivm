<?php
/**
 * @class VMBasePaymentProvider
 * Description of VMBasePaymentProvider class
 *
 * @author Nikita Kolosov <nkolosov@voodoo-mobile.com>
 */
abstract class VMBasePaymentProvider extends CComponent implements VMPaymentProvider {
	protected $paymentId;

	public $errors;

	/**
	 * @param array $options
	 */
	public function setOptions($options = array()) {
		foreach($options as $key => $value) {
			if(property_exists(get_class($this), $key)) {
				$this->{$key} = $value;
			}
		}
	}

	public function getPaymentId() {
		return $this->paymentId;
	}

	/**
	 * @param VMAmount  $amount
	 */
	public function setAmount($amount) {}

	/**
	 * @param VMCreditCard $creditCard
	 */
	public function setCardDetails($creditCard) {}
} 