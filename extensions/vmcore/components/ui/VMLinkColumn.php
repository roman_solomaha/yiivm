<?php


class VMLinkColumn extends TbDataColumn
{
    public $label = null;
    public $htmlOptions = array();

    protected function renderDataCellContent($row, $data)
    {
        $value = null;

        if ($this->label) {
            $label = $this->evaluateExpression($this->label, array('data' => $data, 'row' => $row));
        } else {
            $label = $this->evaluateExpression($this->data, array('data' => $data, 'row' => $row));
        }


        if ($this->value !== null) {
            $value = $this->evaluateExpression($this->value, array('data' => $data, 'row' => $row));
        } elseif ($this->name !== null) {
            $value = CHtml::value($data, $this->name);
        }

        $attributes = array_merge(array('href' => $value), $this->htmlOptions);
        echo CHtml::tag('a', $attributes, $label);
    }
}