<?php

/**
 * @class  VMDependedOptionsLoad
 * Description of VMDependedOptionsLoad class
 * @author Nikita Kolosov <nkolosov@voodoo-mobile.com>
 */
class VMDependedOptionsLoadAction extends CAction {
	/**
	 * @var string $modelName of CActiveRecord
	 * Which objects need respond in drop down list
	 */
	public $modelName = null;
	/**
	 * @var string $requestModel of CModel from POST
	 * Model of form input
	 */
	public $requestModel = null;
	/**
	 * @var string $attributeName
	 * Which form attribute value need to find
	 */
	public $attributeName = null;
	/**
	 * @var string $modelAttributeName
	 * Which model attribute compare with form attribute. Default $attributeName and $modelAttributeName is equal
	 */
	public $modelAttributeName = null;

	public $value = 'id';
	public $name = 'name';

	/**
	 * @var CDbCriteria|null $criteria
	 * Additional criteria for find
	 */
	public $criteria = null;

	public function run () {
		$this->checkParams();

		$model = CActiveRecord::model($this->requestModel);

		if (!$model) {
			throw new CHttpException(500, Yii::t('vmcore.ui', '{className} not found', array('{className}' => $this->requestModel)));
		}

		if (!$this->modelAttributeName) {
			$this->modelAttributeName = $this->attributeName;
		}

		$attributes = (object)Yii::app()->request->getParam($this->requestModel);

		$criteria = new CDbCriteria();
		$criteria->compare($this->modelAttributeName, $attributes->{$this->attributeName});

		if ($this->criteria) {
			$criteria->mergeWith($this->criteria);
		}

		$entityModel = CActiveRecord::model($this->modelName);
		if (!$entityModel) {
			throw new CHttpException(500, Yii::t('vmcore.ui', '{className} not found', array('{className}' => $this->modelName)));
		}

		$entities = $entityModel->findAll($criteria);

		if ($entities) {
			foreach ($entities as $entity) {
				echo CHtml::tag('option', array(
					'value' => $entity->{$this->value}
				), $entity->{$this->name});
			}
		}
	}

	protected function checkParams () {
		if (!$this->modelName) {
			throw new CHttpException(500, Yii::t('vmcore.errors', '{property} is not set up properly', array('{property}' => 'modelName')));
		}

		if (!$this->requestModel) {
			throw new CHttpException(500, Yii::t('vmcore.errors', '{property} is not set up properly', array('{property}' => 'requestModel')));
		}

		if (!$this->attributeName) {
			throw new CHttpException(500, Yii::t('vmcore.errors', '{property} is not set up properly', array('{property}' => 'attributeName')));
		}

		if (!VMEntitySaver::containsEntity($this->requestModel)) {
			throw new CHttpException(400, Yii::t('vmcore.errors', 'There are no parameters matching "{className}"', array(
				'{className}' => $this->requestModel,
			)));
		}
	}
} 