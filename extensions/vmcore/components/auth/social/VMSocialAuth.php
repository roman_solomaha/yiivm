<?php
/**
 * @class VMSocialAuth
 * Description of VMSocialAuth class
 *
 * @author Nikita Kolosov <nkolosov@voodoo-mobile.com>
 */

Yii::setPathOfAlias('vmsocialauth', __DIR__ . DIRECTORY_SEPARATOR);

Yii::import("yiivm.extensions.social-auth.lightopenid.*");
Yii::import("yiivm.extensions.social-auth.eauth.*");
Yii::import("yiivm.extensions.social-auth.eauth.services.*");
Yii::import("yiivm.extensions.social-auth.eoauth.*");
Yii::import("yiivm.extensions.social-auth.eoauth.lib.*");

class VMSocialAuth extends EAuth
{
	public function init()
	{
		Yii::import("vmsocialauth.*");
		Yii::import("vmsocialauth.services.*");
		Yii::import("vmsocialauth.actions.*");

		Yii::app()->setComponent('loid', new loid());

		parent::init();
	}
} 