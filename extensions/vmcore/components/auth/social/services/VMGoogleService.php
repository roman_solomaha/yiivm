<?php
/**
 * @class VMGoogleService
 * Description of VMGoogleService class
 *
 * @author Nikita Kolosov <nkolosov@voodoo-mobile.com>
 */

class VMGoogleService extends GoogleOpenIDService {
	protected $requiredAttributes = array(
		'name' => array('firstname', 'namePerson/first'),
		'lastname' => array('lastname', 'namePerson/last'),
		'email' => array('email', 'contact/email'),
	);

	protected $optionalAttributes = array(
		'country' => array('country', 'contact/country/home'),
		'language' => array('language', 'pref/language'),
	);
}