<?php

class VMAuthAction extends CAction
{
	public $userClass;
	public $authView;

	public function run()
	{
		if (!$this->authView) {
			throw new CException(Yii::t('vmcore.errors', '{property} is not set up properly', array('{property}' => 'authView')));
		}

		if (!$this->userClass) {
			throw new CException(Yii::t('vmcore.errors', '{property} is not set up properly', array('{property}' => 'userClass')));
		}

		$model = new VMAuthForm();
		$model->userClass = $this->userClass;
		$attributes = Yii::app()->request->getParam('VMAuthForm');

		if ($attributes) {
			$model->attributes = $attributes;
			if ($model->validate() && $model->authenticate()) {

				if (!Yii::app()->user->returnUrl) {
					Yii::app()->controller->redirect(array('index'));
				} else {
					Yii::app()->controller->redirect(Yii::app()->user->returnUrl);
				}

			}
		}

		$this->controller->render($this->authView, array('model' => $model));
	}
}