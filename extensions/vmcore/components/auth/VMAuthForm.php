<?php

/**
 * Class VMSignInForm
 */
class VMAuthForm extends CFormModel
{
	public $userClass;
	/**
	 * @var $email . Used for storing the email parameter from the form
	 */
	public $email;
	/**
	 * @var $password . Stores the password entered by a user when signing in
	 */
	public $password;
	/**
	 * @var $rememberMe . Contains the flag that means if the system should remember about the signed in user.
	 */
	public $rememberMe;

	public $identity;

	public $afterUserLogin;
	public $beforeUserLogin;

	/**
	 * @return array
	 */
	public function rules()
	{
		return array(
			array('email, password', 'required'),
			array('email, password', 'length', 'max' => 45),
			array('email', 'email'),
		);
	}

	/**
	 * @return bool
	 */
	public function authenticate()
	{
		$this->identity = new VMEmailIdentity($this->userClass, $this->email, $this->password);

		if (!$this->identity->authenticate()) {
			$this->addError('email', Yii::t('vmcore.auth', 'Incorrect email or password'));
			return false;
		}

		if (!$this->beforeUserLogin || call_user_func($this->beforeUserLogin, new CEvent($this))) {
			Yii::app()->user->login($this->identity);

			if ($this->afterUserLogin) {
				call_user_func($this->afterUserLogin, new CEvent($this));
			}
			return true;
		}

		$this->addError('email', Yii::t('vmcore.auth', 'Incorrect email or password or permission denied'));
		return false;
	}

	public function attributeLabels()
	{
		return array(
			'email' => Yii::t('vmcore.auth', 'Email'),
			'password' => Yii::t('vmcore.auth', 'Password'),
			'rememberMe' => Yii::t('vmcore.auth', 'Remember Me'),
		);
	}
}