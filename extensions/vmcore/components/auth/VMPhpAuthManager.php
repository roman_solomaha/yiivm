<?php
/**
 * @class VMPhpAuthManager
 * Description of VMPhpAuthManager class
 *
 * @author Nikita Kolosov <nkolosov@voodoo-mobile.com>
 */
class VMPhpAuthManager extends CPhpAuthManager {
	public function init() {
		parent::init();

		if (!Yii::app()->user->isGuest && Yii::app()->user->role) {
			$roles = Yii::app()->user->role;

			if(is_array($roles)) {
				foreach($roles as $role) {
					$this->assign($role, Yii::app()->user->id);
				}
			} else {
				$this->assign($roles, Yii::app()->user->id);
			}

		}
	}
} 