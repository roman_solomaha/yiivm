<?php

return array(
    'Notification has been successfully sent' => 'Уведомление успешно отправлено',
    'You must specify a message for sending' => 'Необходимо указать сообщение',
    'You must specify a target for sending' => 'Необходимо указать направление',
    '{className} have not a {field}' => '{className} не имеет поле {field}',
    'You must specify a message and a channel for sending' => 'Необходимо указать сообщение и канал',
);