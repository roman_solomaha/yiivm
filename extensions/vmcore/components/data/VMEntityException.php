<?php

class VMEntityException extends VMModelException
{
	public function __construct(CActiveRecord $entity)
	{
		parent::__construct($entity);
	}
}