<?php

class VMModelEntity extends CModel
{
	public function attributeNames()
	{
		$class = new ReflectionClass(get_class($this));
		$names = array();
		foreach ($class->getProperties() as $property) {
			$name = $property->getName();
			if ($property->isPublic() && !$property->isStatic())
				$names[] = $name;
		}
		return $names;
	}
}