<?php

class VMAssetPublisher extends CComponent
{
	public $cssFolder = 'css';
	public $jsFolder = 'js';
	protected $assetsUrl = null;

	public function publish($path)
	{
		$this->assetsUrl = Yii::app()->assetManager->publish($path, false, -1, YII_DEBUG);

		$this->registerCss($path);
		$this->registerScripts($path);
	}

	protected function registerCss($path)
	{
		$files = $this->findFiles($path . DIRECTORY_SEPARATOR . $this->cssFolder, 'css');

		if(!$files) {
			return;
		}

		foreach ($files as $file) {
			$pathInfo = pathinfo($file);
			Yii::app()->clientScript->registerCssFile($this->assetsUrl . '/' . $this->cssFolder . '/' . $pathInfo['basename']);
		}
	}

	protected function findFiles($path, $ext)
	{
		if(!file_exists($path) || !is_dir($path)) {
			return false;
		}

		return CFileHelper::findFiles($path, array(
			'fileTypes' => array($ext),
			'level' => -1
		));
	}

	protected function registerScripts($path)
	{
		$files = $this->findFiles($path . DIRECTORY_SEPARATOR . $this->jsFolder, 'js');
		if (!$files) {
			return;
		}

		foreach ($files as $file) {
			$pathInfo = pathinfo($file);
			Yii::app()->clientScript->registerScriptFile($this->assetsUrl . '/' . $this->jsFolder . '/' . $pathInfo['basename']);
		}
	}
}