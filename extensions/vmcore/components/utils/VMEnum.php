<?php

abstract class VMEnum
{
	/**
	 * @param $const
	 *
	 * @return stdClass
	 * @throws CException
	 */
	public static function get($const)
	{
		$meta = static::meta();
		if (!array_key_exists($const, $meta)) {
			throw new CException(Yii::t('vmcore.utils', 'Invalid constant'));
		}

		$properties = array_keys($meta[$const]);
		$result = new stdClass();

		foreach($properties as $property) {
			$value = $meta[$const][$property];

			$result->{$property} = (is_object($value) && get_class($value) === 'Closure')
				? call_user_func($value)
				: $value;
		}

		return $result;
	}

	/**
	 * @param string $class
	 *
	 * @return array
	 */
	public static function getConstantsMap()
	{
		$reflection = new ReflectionClass(get_called_class());
		return $reflection->getConstants();
	}

	/**
	 * @return array
	 */
	protected static function meta() {}

	/**
	 * @param null|string $valueField
	 * @param string $labelField
	 *
	 * @return array
	 * @throws CException
	 */
	public static function listData($valueField = null, $labelField = null) {
		if(!$labelField) {
			throw new CException(Yii::t('vmcore.errors', '{property} is not set up properly', array('{property}' => 'label')));
		}

		$result = array();

		$reflection = new ReflectionClass(get_called_class());
		$constants = $reflection->getConstants();

		foreach($constants as $constValue) {
			$data = static::get($constValue);
			if($valueField && !isset($data->{$valueField})) {
				throw new CException(Yii::t('vmcore.errors', '{property} is not set up properly', array('{property}' => 'value')));
			}

			if(!isset($data->{$labelField})) {
				throw new CException(Yii::t('vmcore.errors', '{property} is not set up properly', array('{property}' => 'label')));
			}

			$key = ($valueField) ? $data->{$valueField} : $constValue;
			$result[$key] = $data->{$labelField};
		}

		return $result;
	}
}