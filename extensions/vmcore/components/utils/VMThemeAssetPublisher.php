<?php

class VMThemeAssetPublisher extends CApplicationComponent
{
	public $sharedFolder = 'shared';
	public $includeCoreAssets = false;
	public $includeModulesAssets = false;

	protected $corePath = null;
	protected $sharedPath = null;
	protected $sharedModulePath = null;
	protected $themePath = null;
	protected $modulePath = null;
	protected $publisher = null;

	public function init()
	{
		$this->publisher = new VMAssetPublisher();

		if($this->includeCoreAssets) {
			$this->corePath = $this->publishFolder('assets', Yii::getPathOfAlias('yiivm.extensions.vmcore'));
		}

		$this->sharedPath = $this->publishFolder($this->sharedFolder . DIRECTORY_SEPARATOR . 'assets');
		$this->themePath  = $this->publishFolder(Yii::app()->theme->name . DIRECTORY_SEPARATOR . 'assets');

		if($this->includeModulesAssets) {
			$this->sharedModulePath = $this->publishFolder($this->sharedFolder . DIRECTORY_SEPARATOR . Yii::app()->controller->module->id . DIRECTORY_SEPARATOR . 'assets');
			$this->modulePath = $this->publishFolder(Yii::app()->theme->name . DIRECTORY_SEPARATOR . Yii::app()->controller->module->id . DIRECTORY_SEPARATOR . 'assets');
		}
	}

	protected function publishFolder($folder, $basePath = null)
	{
		if(!$basePath) {
			$basePath = Yii::app()->themeManager->basePath;
		}

		$path = $basePath . DIRECTORY_SEPARATOR . $folder;

		if (file_exists($path) && is_dir($path)) {
			$this->publisher->publish($path);
		}

		return $path;
	}

	public function getSharedUrl()
	{
		return Yii::app()->assetManager->getPublishedUrl($this->sharedPath);
	}

	public function getThemeUrl()
	{
		return Yii::app()->assetManager->getPublishedUrl($this->themePath);
	}

	public function getCoreUrl()
	{
		return Yii::app()->assetManager->getPublishedUrl($this->corePath);
	}

	public function getModuleUrl()
	{
		return Yii::app()->assetManager->getPublishedUrl($this->modulePath);
	}

	public function getSharedModuleUrl()
	{
		return Yii::app()->assetManager->getPublishedUrl($this->sharedModulePath);
	}
}