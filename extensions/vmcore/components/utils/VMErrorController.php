<?php

class VMErrorController extends CController
{
	public $errorView = 'vmcore.views.error.index';
	public $layout = '//default';

	public function actionIndex()
	{
		if ($error = Yii::app()->errorHandler->error) {
			if (Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else {
				$this->render($this->errorView, $error);
			}
		}

	}
}