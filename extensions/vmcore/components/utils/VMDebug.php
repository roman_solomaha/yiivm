<?php

/**
 * Class VMDebug
 */
class VMDebug extends CComponent {
	/**
	 * @return Token|null
	 */
	public static function getToken() {
		return Token::model()->find(array('order' => 'id DESC'));
	}
}