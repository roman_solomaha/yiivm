<?php
/**
 * @class VMPushManager
 * VMPushManager class
 *
 * @author Nikita Kolosov <nkolosov@voodoo-mobile.com>
 */

class VMPushManager {
	/**
	 * @var integer Push using parse.com
	 */
	const PROVIDER_PARSE  = 1;
	/**
	 * @var integer Push using xtreme.com
	 */
	const PROVIDER_XTREME = 2;
	/**
	 * @var integer Push using Amazon SNS
	 */
	const PROVIDER_AMAZON = 3;

	/**
	 * @var VMPushProvider $provider
	 */
	public $provider;

	/**
	 * @param integer $provider
	 */
	public function __construct($provider, $config = array()) {
		if($provider instanceof VMPushProvider) {
			$this->provider = $provider;
		} else {
			switch($provider) {
				case self::PROVIDER_PARSE:
					$this->provider = new VMParsePushProvider($config);
					break;
				case self::PROVIDER_XTREME:
					$this->provider = new VMXtremePushProvider($config);
					break;
				case self::PROVIDER_AMAZON:
					$this->provider = new VMAmazonPushProvider($config);
					break;
			}
		}
	}

	/**
	 * @param string $message Push message
	 * @param array  $data    Payloads data
	 *
	 * @return boolean
	 */
	public function sendPush($message, $data = null) {
		return $this->provider->sendPush($message, $data);
	}

	/**
	 * @return integer
	 */
	public function getCode() {
		return $this->provider->getCode();
	}
}