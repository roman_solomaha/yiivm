<?php
/**
 * @class VMPushProvider
 * VMPushProvider class
 *
 * @author Nikita Kolosov <nkolosov@voodoo-mobile.com>
 */
interface VMPushProvider
{
	const STATUS_OK = 1;
	const STATUS_BAD_PARAM = 2;

	/**
	 * Set application key for provider
	 *
	 * @param string $applicationKey
	 * @return void
	 */
	public function setApplicationKey($applicationKey);

	/**
	 * Set API key or API-token for provider
	 *
	 * @param string $apiKey
	 * @return void
	 */
	public function setApiKey($apiKey);

	/**
	 * Send push notification to devices
	 *
	 * @param string $message Message for sending
	 *
	 * @return boolean true if successful or false otherwise
	 * @deprecated
	 */
	public function sendPush($message);

	/**
	 * Send push notification to devices
	 *
	 * @param VMPushModel $data Message for sending
	 *
	 * @return boolean true if successful or false otherwise
	 */
	public function sendRichPush(VMPushModel $data);

	/**
	 * Return a server response if exists
	 *
	 * @return mixed
	 */
	public function getResponse();

	/**
	 * Return a errors if exists
	 *
	 * @return mixed
	 */
	public function getErrors();

	/**
	 * Return a status code
	 *
	 * @return integer
	 */
	public function getCode();

	/**
	 * Set response code and response message
	 *
	 * @param integer $code
	 * @param string $message
	 * @param mixed $data
	 *
	 * @return void
	 */
	public function setResponse($code, $message, $data = array());
}