<?php

abstract class VMConfigDetector extends CComponent
{
	protected $params;
	protected $name = null;

	public function __construct($name, $params = array())
	{
		$this->name = $name;
		$this->params = $params != null ? VMObjectUtils::fromArray($params) : null;
	}

	public abstract function detected();
}