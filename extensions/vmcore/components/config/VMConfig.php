<?php

class VMConfig extends CComponent
{
	const DEFAULT_DETECTOR = 'VMFileConfigDetector';
	public $detector = null;
	public $options = array();
	public $modifiers = array();
	private $name = null;

	public function __construct($name, $params)
	{
		$this->name = $name;
		$params = VMObjectUtils::fromArray($params);

		$this->options = isset($params->options) ? $params->options : null;
		$this->modifiers = isset($params->modifiers) ? $params->modifiers : null;
		$detectorClass = null;

		if (isset($params->detector)) {
			$detectorOptions = $params->detector;

			if (isset($detectorOptions->class)) {
				$detectorClass = $detectorOptions->class;
			}
		}

		if (!$detectorClass) {
			$detectorClass = self::DEFAULT_DETECTOR;
		}

		$this->detector = new $detectorClass($name, $this->options);
	}

	public function isActive()
	{
		return $this->detector->detected();
	}

	public function run()
	{
		if($this->options) {
			foreach (VMObjectUtils::toArray($this->options) as $key => $value) {
				$this->setValue(Yii::app(), $key, $value);
			}
		}

		if($this->modifiers) {
			foreach (VMObjectUtils::toArray($this->modifiers) as $modifierOptions) {
				$modifierClass = $modifierOptions['class'];
				$modifier = new $modifierClass;
				foreach ($modifierOptions as $option => $value) {
					if ($option !== 'class') {
						$modifier->{$option} = $value;
					}
				}
				$modifier->run();
			}
		}
	}

	public function setValue(CComponent $component, $key, $value)
	{
		if (is_array($value) && is_a($component->{$key}, 'CComponent')) {
			foreach ($value as $subKey => $subValue) {
				$this->setValue($component->{$key}, $subKey, $subValue);
			}
		} else {
			$component->{$key} = $value;
		}
	}
}