<?php

abstract class VMConfigModifier extends CComponent
{
	public abstract function run();
}