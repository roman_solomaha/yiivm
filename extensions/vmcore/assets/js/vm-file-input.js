/**
 * Created with JetBrains PhpStorm.
 * User: egor
 * Date: 05.12.13
 * Time: 3:06
 * To change this template use File | Settings | File Templates.
 */
/* jsHint browser:true*/
/* global window:true */
(function ($, Error, undefined) {
    'use strict';

    /**
     * @class VMFileInput
     * @constructor
     * @this {VMFileInput}
     * @param {jQuery} jQ
     * @param {Object} options
     * */
    function VMFileInput(jQ, options) {
        this.$ = jQ;
        this.options = $.extend({}, this.$default.options, options);
    }

    var prototype = VMFileInput.prototype;

    prototype.$default = {
        options: {
        }
    };

    prototype.buildPreview = function (options) {
        options = $.extend({availableTypes: ['image']}, this.options, options);
        options.name = this.$.attr('name');

        var that = this;
        $.map(this.loadContent(options), function (deferred) {
            if (!deferred) {
                throw new Error('unavailable type');
            }
            deferred.then(
                that.$onReadPreview.bind(that, options)
            );
        });
    };

    prototype.getFilename = function () {
        return $.map(this.$.get(0).files, function (file) {
            return file.name;
        });
    };

    prototype.loadContent = function (options) {
        options = options || {};
        return $.map(
            this.$.get(0).files,
            this.$loadContent(options)
        );
    };

    prototype.$loadContent = function (options) {
        return function (options, file) {
            var deferred = new $.Deferred();

            var isAvailableType = $.map(options.availableTypes || {},function (type) {
                if ($.isString(type)) {
                    type = new RegExp(type + '.*');
                }
                return file.type.match(type);
            }).indexOf(false);

            if (-1 !== isAvailableType) {
                return null;
            }
            var reader = new window.FileReader();
            reader.onload = function (event) {
                deferred.resolve(event);
            };
            reader.onerror = function (event) {
                deferred.reject(event);
            };
            reader.readAsDataURL(file);

            return deferred;
        }.bind(this, options);
    };

    prototype.$onLoadPreview = function (container, context, options) {
        container.data('realWidth', context.width);
        container.data('realHeight', context.height);
        container.prop('src', this.src);
        var callback = options.afterLoadPreview;
        if (callback !== undefined && $.isFunction(callback)) {
            callback.call(container, {target: container});
        }
    };

    prototype.$onReadPreview = function (options, event) {
        var container = $('<img/>');
        var context = new window.Image();
        context.onload = VMFileInput.prototype.$onLoadPreview.bind(this, container, context, options);
        container.prop('src', event.target.result);
        container.prop('name', options.name);
        context.src = event.target.result;
    };

    $.fn.vmFileInput = function (options) {
        if (!$.vmFileInput.isSetFileApi()) {
            throw new Error('Old browser: javascript file api not found');
        }

        if (this.length > 1) {
            options = Array.prototype.slice.call(arguments);
            return this.each(function () {
                $.fn.vmFileInput.apply($(this), options);
            });
        }

        if (this.get(0).tagName !== 'INPUT' || this.attr('type') !== 'file') {
            throw new Error('this is not file input');
        }

        var command;
        if ($.isString(options)) {
            command = options;
            options = Array.prototype.slice.call(arguments, 1);
        }

        var vmFileInput = this.data($.fn.vmFileInput.dataKey);

        if (vmFileInput === undefined) {
            vmFileInput = new VMFileInput(this, command ? options[0] : options);
            this.each(function () {
                $.data(this, $.fn.vmFileInput.dataKey, vmFileInput);
            });
        }

        if (command !== undefined) {
            command = command.replace('$', '');
            if ($.isFunction(vmFileInput[command])) {
                return vmFileInput[command].apply(vmFileInput, options);
            }
        }

        return this;
    };

    $.fn.vmFileInput.dataKey = 'vmFileInput';
    $.fn.vmFileInput.default = {};

    $.vmFileInput = {};
    $.vmFileInput.isSetFileApi = function () {
        return typeof Blob !== undefined &&
            typeof FileReader !== undefined &&
            typeof FormData !== undefined &&
            typeof File !== undefined &&
            typeof FileList !== undefined;
    };
})(window.jQuery, window.Error);