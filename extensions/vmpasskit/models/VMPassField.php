<?php

class VMPassField extends VMModelEntity
{
	public $key;
	public $label;
	public $value;

	public function __construct($key, $value = null, $label = null)
	{
		$this->label = $label;
		$this->value = $value;
		$this->key = $key;
	}
}