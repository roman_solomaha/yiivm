<?php

class VMBoardingPassContent extends VMModelEntity
{
	public $transitType = VMBoardingPassTransitType::PK_TRANSIT_TYPE_AIR;
	public $headerFields = array();
	public $primaryFields = array();
	public $secondaryFields = array();
	public $auxiliaryFields = array();

	public function getBoardingAt()
	{
		return $this->getProperty('headerFields', 'boarding-at', 'Boarding');
	}

	public function getProperty($section, $key, $label = null)
	{
		foreach ($this->{$section} as $field) {
			if ($field->key === $key) {
				return $field;
			}
		}

		$field = new VMPassField($key, null, $label);
		array_push($this->{$section}, $field);
		return $field;
	}

	public function getFlight()
	{
		return $this->getProperty('secondaryFields', 'flight', 'Flight');
	}

	public function getDepartureDate()
	{
		return $this->getProperty('secondaryFields', 'departure-date', 'Date');
	}

	public function getGroup()
	{
		return $this->getProperty('secondaryFields', 'group', 'group');
	}

	public function getSeat()
	{
		return $this->getProperty('secondaryFields', 'seat', 'Seat');
	}

	public function getBoardingGate()
	{
		return $this->getProperty('headerFields', 'boarding-gate', 'Gate');
	}

	public function getPassenger()
	{
		return $this->getProperty('auxiliaryFields', 'passenger-name', 'Passenger');
	}

	public function getOrigin()
	{
		return $this->getProperty('primaryFields', 'origin');
	}

	public function getDestination()
	{
		return $this->getProperty('primaryFields', 'destination');
	}
}